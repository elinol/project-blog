<?php
/*
    Handling user data in database.
    Author: Elin Olsson, 2021.
*/

class User {
    // CLASS MEMBERS -------------------------------------------
    private $db;
    private $id;
    private $username;
    private $fullname;
    private $password;
    private $created_at;

    // Constructor
    public function __construct($id = null) 
    {
        $this->db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATABASE); 
        if ($this->db->connect_errno > 0) {
            die("Error on connection: " . $this->db->connect_errno);
        }

        // Get an existing user object
        if (!empty($id)) {
            $id = intval($id);
            $user       = $this->get_user($id);
            $this->id   = $user['id'];
            $this->username = $user['username'];
            $this->fullname = $user['fullname'];
            $this->created_at = $user['created_at'];
        }

        return $this;
    }

    // Destructor
    function __destruct()
    {
        mysqli_close($this->db);
    }

    // GETTERS --------------------------------------------------
    function get_username() {
        return $this->username;
    }
    function get_fullname() {
        return $this->fullname;
    }

    function get_id() {
        return $this->id;
    }

    function get_date() {
        return $this->created_at;
    }

    // SETTERS ---------------------------------------------------
    function set_id($id) {
        $id = intval($id);
        if ($id) {
            $this->id = $id;
            return true;
        } else {
            return false;
        }
    }

    function set_username($username) {
        if($username != "") {
            $this->username = $this->db->real_escape_string($username);
            return true;
        } else {
            return false;
        }
    }

    function set_fullname($fullname) {
        if($fullname != "") {
            $this->fullname = $this->db->real_escape_string($fullname);
            return true;
        } else {
            return false;
        }
    }

    function set_password($password) {
        if($password != "") {
            $this->password = sha1($this->db->real_escape_string($password));
            return true;
        } else {
            return false;
        }
    }

    // CRUD operations -------------------------------------------
    
    // Add User
    function add_user($username, $fullname, $password) {
        if(!$this->set_username($username)) { return false; }
        if(!$this->set_fullname($fullname)) { return false; }
        if(!$this->set_password($password)) { return false; }

        $query  = 'INSERT INTO users (created_at, username, fullname, password) '
                . 'VALUES (NOW(), "' . $this->username . '", "' . $this->fullname . '", "' . $this->password . '");';

        $result = $this->db->query($query);
        return $result;
    }

    // Delete User
    function delete_user($id) {
        $query = 'DELETE FROM users WHERE id = "' . $id . '"';
        return ($this->db->query($query));
    }
    
    // Get User by ID
    public function get_user($id)
    {
        $query = 'SELECT * FROM users WHERE id = "' . $id . '"';
        $result = $this->db->query($query);
        return mysqli_fetch_assoc($result);
    }


    // Get all users
    public function get_users()
    {
        $query = 'SELECT * FROM users';
        $result = $this->db->query($query);

        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    // Check if username and password match
    public function verify_password()
    {
        $query  = 'SELECT * FROM users '
            . 'WHERE username = "' . $this->username . '" '
            . 'AND password = "' . $this->password . '"';

        return ($this->db->query($query)->fetch_object());
    }

    public function get_user_id($username) {
        $query = 'SELECT  id FROM users WHERE username = "' . $username . '"';
        $result = $this->db->query($query);
        return mysqli_fetch_assoc($result);
    }
}