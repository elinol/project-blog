<?php
/*
    Handles post data in database.
    Author: Elin Olsson, 2021.
*/
class Post {

    private $db;
    private $title;
    private $body;
    private $img_path;
    private $id;
    private $user_id;
    private $created_at;
    private $username;
    private $fullname;


    // Constructor
    function __construct($id = null) {
        $this->db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATABASE); 
        if ($this->db->connect_errno > 0) {
            die("Error on connection: " . $this->db->connect_errno);
        }

        // Get an existing post object
        if (!empty($id)) {
            $id = intval($id);
            $post       = $this->get_post($id);
            $this->id   = $post['id'];
            $this->username = $post['username'];
            $this->fullname = $post['fullname'];
            $this->user_id = $post['user_id'];
            $this->title = $post['title'];
            $this->body = $post['body'];
            $this->img_path = $post['img_path'];
            $this->created_at = $post['created_at'];
        }
    }

    function __destruct()
    {
        mysqli_close($this->db);
    }

    function get_id() {
        return $this->id;
    }

    function get_username() {
        return $this->username;
    }
    function get_user_id() {
        return $this->user_id;
    }

    function get_fullname() {
        return $this->fullname;
    }
    function get_title() {
        return $this->title;
    }
    function get_body() {
        return $this->body;
    }
    function get_date() {
        return $this->created_at;
    }
    function get_img() {
        return $this->img_path;
    }

    

    // SETTERS
    function set_title($title) {
        if($title != "") {
            $this->title = $this->db->real_escape_string($title);
            return true;
        } else {
            return false;
        }
    }

    function set_body($body) {
        if($body != "") {
            $this->body = $this->db->real_escape_string($body);
            return true;
        } else {
            return false;
        }
    }
    function set_img($path) {
        if($path != "") {
            $this->img_path = $this->db->real_escape_string($path);
        } else {
            $this->img_path = NULL;
        }

        return true;
    }

    // CRUD OPERATIONS _________________________________________________________________________________________
    // Get ALL posts
    function get_all_posts() {

        $sql = "SELECT posts.*, users.username, users.fullname FROM posts INNER JOIN users 
        ON posts.user_id=users.id ORDER BY id DESC";
        $result = $this->db->query($sql);
        
        
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    // Get posts from specific user
    function get_posts($id) {
        $id = intval($id);
        $sql = "SELECT posts.*, users.username, users.fullname FROM posts  INNER JOIN users 
        ON posts.user_id=users.id WHERE user_id='$id' ORDER BY id DESC";

        $result = $this->db->query($sql);
        if (mysqli_num_rows($result) == 0){
            return false;
        } 
        
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }


    // Get specific post
    function get_post($id) {
        $sql = "SELECT posts.*, users.username, users.fullname FROM posts INNER JOIN users 
        ON posts.user_id=users.id WHERE posts.id=$id";
        $result = $this->db->query($sql);

        if (!$result) {
            //return "Post could not be found.";
            return false;
        }

        return mysqli_fetch_assoc($result);
    }

    // Add Post
    function add_post($title, $body, $img_path, $user_id){
        if(!$this->set_title($title)) { return false; }
        if(!$this->set_body($body)) { return false; }
        if(!$this->set_img($img_path)) { return false; }
        
        $sql = "INSERT INTO posts(title, body, img_path, created_at, user_id)values
        ('" . $this->title . "', '" . $this->body . "', '" . $this->img_path . "', NOW(), '".$user_id."');";
        $result = $this->db->query($sql);

        return $result;
    }

    // Delete post by id
    function delete_post($id) {
        $sql = "DELETE FROM posts WHERE id=$id";
        return $this->db->query($sql);

        $result = $this->db->query($sql);
        return $result;
    }

    // Update post
    function updatePost($id, $title, $body) {
        $id = intval($id);
        if(!$this->set_title($title)) { return false; }
        if(!$this->set_body($body)) { return false; }

        $sql = "UPDATE posts SET title='". $this->title."', body='".$this->body."' WHERE id='".$id."'";
        $result = $this->db->query($sql);
        return $result;
    }

}
