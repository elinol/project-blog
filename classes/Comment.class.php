<?php
/*
    Class for handling blog post comments.
    Author: Elin Olsson,  2021
*/

class Comment
{
    // Members
    private $body;
    private $db;



    // Constructor
    function __construct()
    {
        $this->db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATABASE);
        if ($this->db->connect_errno > 0) {
            die("Error on connection: " . $this->db->connect_errno);
        }
    }

    // Destructor
    function __destruct()
    {
        mysqli_close($this->db);
    }

    // Setters
    function set_body($body)
    {
        if ($body != "") {
            $this->body = $this->db->real_escape_string($body);
            return true;
        } else {
            return false;
        }
    }

    // CRUD OPERATIONS ------------------------------------------------------------

    // Get comments on post by id
    function get_post_comments($id)
    {
        $sql = "SELECT comments.*, users.username FROM comments INNER JOIN users ON comments.user_id=users.id 
        WHERE comments.post_id=$id";
        $result = $this->db->query($sql);

        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    // Add Comment
    function create_comment($body, $user_id, $post_id)
    {
        if (!$this->set_body($body)) {
            return false;
        }

        $sql = "INSERT INTO comments(body, created_at, user_id, post_id)values('" . $this->body . "', NOW(), '" . $user_id . "', '" . $post_id . "');";
        $result = $this->db->query($sql);
        return $result;
    }

    // Delete comments from specific post
    function delete_comments($post_id)
    {
        $sql = "DELETE FROM comments WHERE post_id=$post_id";
        $result = $this->db->query($sql);
        return $result;
    }
}
