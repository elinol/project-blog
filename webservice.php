<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/config.php");

$posts = New Posts();

header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");

$rows = $posts->get_posts();
$json = json_encode($rows, JSON_PRETTY_PRINT);

echo $json;