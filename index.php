<?php
include_once("./includes/config.php");
include("pages.php");       // For routing


// Check if page is set in query, if not: set index as page
// (Page is set in mainmenu.php)
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 'index';
}

// Get page data from pages array in pages.php
$page_data = get_page($page);

// Render header with page data (for setting title etc.)
render_header($page_data);

// Render page template
if ($page_data) {
    echo render_page($page_data);
}

// Render footer
render_footer($page_data);
