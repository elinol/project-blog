// Toggle display of Add Post form
function showDiv() {
    var x = document.getElementById("add-form");
    var y = document.getElementById("add-form-toggler");

    console.log(x.style.display)
    if (x.style.display === "" || x.style.display === "none") {
        x.style.display = "block";
        y.setAttribute("value", "Cancel");
    } else {
        x.style.display = "none";
        y.setAttribute("value", "Add Post");
    }
}

// Search function on start and blog pages
function searchUser() {
    var txtValue;
    var input = document.getElementById('search-user');
    input = input.value.toUpperCase();

    var items = document.getElementsByClassName('user-link');
    for (i = 0; i < items.length; i++) {
        txtValue = items[i].innerText;
        if (txtValue.toUpperCase().indexOf(input) > -1) {
            items[i].style.display = "";
        } else {
            items[i].style.display = "none";
        }
    }

}

// File extension validation on file upload
function checkFileExtension(filename) {
    allowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];

    // Get extension from filename
    fileExt = filename.split('.').pop();


    // Check if it's jpg, jpgeg, png or gif
    if (allowedExtensions.indexOf(fileExt) > -1) {
        return true;
    } else {
        document.getElementById("warning").innerHTML = "File type not allowed.";
        return false;
    }
}

// Size validation on file upload.
function checkFileSize(filesize) {
    if (filesize > 2097152) {
        document.getElementById("warning").innerHTML = "File is too big.";
        return false;
    } else {
        return true;
    }
}

// FILE UPLOAD
const fileUploadUrl = "https://api.cloudinary.com/v1_1/dbt92fxk7/image/upload";
const fileinput = document.querySelector("#image-input");

// Listen for change in file input
if (fileinput) {
    fileinput.addEventListener("change", (e) => {
        e.preventDefault();

        const file = document.querySelector("[type=file]").files[0];
        // Check if file extension is ok. If false return error message
        if (!checkFileExtension(file.name)) {
            return false;
        }
        //console.log(checkFileExtension(files[0].name))
        if (!checkFileSize(file.size)) {
            return false;
        }

        document.getElementById("warning").innerHTML = "Wait a sec..";

        const formData = new FormData();
        formData.append("file", file);
        formData.append("upload_preset", "idj5rse6");

        fetch(fileUploadUrl, {
            method: "POST",
            body: formData
        })
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                console.log(data.secure_url)
                document.getElementById("image-url").value = data.secure_url;
                document.getElementById("warning").innerHTML = "File is ok!";
            });
    });
}

