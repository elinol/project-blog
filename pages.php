<?php

$pages = [
    'index' => [
        'title' => 'Start',
        'template_file' => 'index.html.php',
    ],
    
    'blog' => [
        'title' => 'Blog',
        'template_file' => 'blog.html.php',
    ],

    'blogpost' => [
        'title' => 'Blogpost', 
        'template_file' => 'post.html.php',
    ],

    'edit' => [
        'title' => 'Edit Post',
        'template_file' => 'edit.html.php',
    ],

    'login' => [
        'title' => 'Log In',
        'template_file' => 'login.html.php',
    ],
    'logout' => [
        'title' => 'Log Out',
        'template_file' => 'logout.php',
    ],

    'user' => [
        'title' => 'User Page',
        'template_file' => 'user.html.php',
    ],
];

function get_page($page_slug) {
    global $pages;
    if (isset($pages[$page_slug])){
        return $pages[$page_slug];
    } else {
        return false;
    }
}

function render_page($page_data) {
    if (isset($page_data['template_file'])) {
        $data = $page_data;
        include('templates/' . $page_data['template_file']);
    }
}

function render_header($page_data) {
    $data = $page_data;
    include('templates/header.php');
}

function render_footer($page_data) {
    $data = $page_data;
    include('templates/footer.php'); 
}

?>