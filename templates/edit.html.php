<?php 
// Check if user is logged in
if (!isset($_SESSION['username'])) {
    header("Location: index.php?page=login");
}

$message = "";
$error = "";
$post = New Post($_GET['id']);


$id = $post->get_id();
$title = $post->get_title();
$body = $post->get_body();
$created_at = $post->get_date();

if(isset($_POST['edited'])) {
    if(!empty($_POST['title'])){
        $title = $_POST['title'];
    }
    if(!empty($_POST['body'])){
        $body = $_POST['body'];
    }


    if ($post->updatePost($id, $title, $body)) {
        $_SESSION['message'] = "Post Updated!";
        header("Location: index.php?page=user&user_id=".$_SESSION['user_id']);
        $error = "";
    } else {
        $message = "Error when updating post!";
        $error = "error";
    }
    
}
?>

<div id="page-content">
<h1>Edit post</h1>
<p class="message <?php echo $error; ?>"><?php echo $message; ?></p>

<form method="POST">
    <input type="text" name="title" placeholder="<?php echo $title; ?>">
    <textarea name="body"><?php echo $body; ?></textarea>
    <input name="edited" type="submit" value="Save">
</form>
</div>