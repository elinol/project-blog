
<p class="banner-text">Welcome to the blog of code enthusiasts!<p>
<div class="row">
    <!-- Blog post feed -->
    <div id="blogposts" class="eight columns">
        <h1 class="h1-margin-bottom">Latest Posts</h1>
        <?php render_posts(); ?>
    </div>

    <!-- User List -->
    <div id="user-list" class="three columns">
        <h2>All Users</h2>
        <input type="text" id="search-user" onkeyup="searchUser()" placeholder="Find user.."><br>
        <?php render_userlist(); ?>
    </div>
</div>