<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.php"); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/functions.php"); ?>
<!DOCTYPE html>
<html lang="sv">

<head>
    <title><?= SITE_TITLE . DIVIDER . $data['title']  ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/normalize.css" type="text/css">
    <link rel="stylesheet" href="../css/skeleton.css" type="text/css">
    <link rel="stylesheet" href="../css/styles.css" type="text/css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>
    <div id="container">

        <header id="mainheader">
            <img id="header-logo" src="../images/logo.svg" alt="Main logotype.">
            <?php include("mainmenu.html.php") ?>
        </header>

        <div id="page-wrapper" class="container">