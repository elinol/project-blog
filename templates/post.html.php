<?php
$post = new Post($_GET['id']);
$message = "";


if (isset($_POST['comment'])) {
    $message = add_comment($_POST['comment']);
}
?>

    <h1><?php echo $post->get_title(); ?></h1>
    <p><em><?php echo $post->get_date(); ?></em> | Author: <?php echo $post->get_fullname(); ?> 
    (<a href='index.php?page=user&user_id=<?php echo $post->get_user_id(); ?>'><?php echo $post->get_username(); ?></a>)</p>
    <?php if(!empty($post->get_img())){ ?>
    <img class="post-img-big" src="<?php echo $post->get_img(); ?>" alt="Full size blog post image."><?php }?>
    <div class="row">
    <p class="eight columns"><?php echo $post->get_body(); ?></p>
    </div>

    <div id="comments">
        <p id="comments-header">Comments</p>
        <?php render_comments($_GET['id']); ?> 
    
        <?php 
        // Show Add Comment form if a user is logged in
        if(array_key_exists('username', $_SESSION)) {
           echo "<p class='error'>".$message."</p>";
           include(ROOT."/templates/comment_form.html.php");
        
        } else {
         echo "<p><a href='index.php?page=login'>Login or Sign Up</a> to add comments.</p>";
        }
        ?>
    </div>




