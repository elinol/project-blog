<?php

// Create objects for User and Posts
$user = new User($_GET['user_id']);
$logged_in = false;
$message = "";

if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

// Check if user is logged in
if (array_key_exists('username', $_SESSION)) {
    if ($user->get_username() == $_SESSION['username']) {
        $logged_in = true;
    }
}

// Add blog post
if (isset($_POST['title'])) {
    $message = add_post();
}

// Delete blog post
if (isset($_GET['deleteid'])) {
    $message = delete_post($_GET['deleteid']);
    unset($_GET['deleteid']);
}
?>
<div id="user-info">
    <h1 class="h1-margin-bottom"><?= $user->get_username() ?></h1>
    <h2><?= $user->get_fullname() ?></h2>
    <p class='center'>Member since <?= $user->get_date() ?></p>
    <p class="error"><?php echo $message ?></p>
    <?php
    // Show Add Post-form if user is logged in
    if ($logged_in) {
        include_once($_SERVER['DOCUMENT_ROOT'] . "/templates/add_post_form.html.php");
    } ?>
</div>

<div  class="row">
    <div class="eight columns">
        <?php
        // Show users posts
        render_posts($user->get_id());
        ?>
    </div>
</div>