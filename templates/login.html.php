<?php
$register_message = "";
$login_message = "";

if (isset($_POST['submit_login']))
    $login_message = login();


if (isset($_POST['submit_reg']))
    $register_message = register_user();
?>



<div class="row">
    <div class="seven columns">
        
        <h1>Log In</h1>
        
        <form method="POST">
        <p class="error"><?php echo $login_message ?></p>
            <div class="row">
                <div class="six columns">
                    <label for="username">Username</label>
                    <input class="u-full-width" type="text" id="username" name="username">
                </div>
            </div>
            <div class="row">
                <div class="six columns">
                    <label for="password">Password</label>
                    <input class="u-full-width" type="password" id="password" name="password">
                </div>
            </div>
            <input class="button-primary" type="submit" value="Login" name="submit_login">
        </form>
    </div>



    <div id="registration-form" class="five columns">

        <h2>User Registration</h2>
        <p>Not a member? Sign up here!</p>
        <form method="POST">
        <p class="error"><?php echo $register_message; ?></p>
            <div class="row">
                <div >
                    <label for="username-reg">Username</label>
                    <input class="u-full-width" type="text"  id="username-reg" name="username">
                </div>
            </div>
            <div class="row">
                <div >
                    <label for="fullname">Full Name</label>
                    <input class="u-full-width" type="text" id="fullname" name="fullname">
                </div>
            </div>
            <div class="row">
                <div>
                    <label for="password-reg">Password</label>
                    <input class="u-full-width" type="password" id="password-reg" name="password">
                </div>
            </div>
            <div class="row">
                <div>
                    <label for="confirm">Confirm Password</label>
                    <input class="u-full-width" type="password"  id="confirm" name="confirm">
                </div>
            </div>
            
            <input class="button-primary" type="submit" value="Submit" name="submit_reg">
        </form>
    </div>
    
</div>
