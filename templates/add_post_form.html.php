<div class="row">
<div id="add-form" class="ten columns offset-by-one">
    <h2 id="add-post-heading">Add Blog Post</h2>
    <form method="POST" enctype="multipart/form-data">
        <input id="add-title" type="text" name="title" placeholder="Title">
        <textarea id="add-body" name="body" placeholder="Body"></textarea>
        <label id="upload-label" for="image-input">Upload an image (JPG, GIF or PNG). Max 2MB.</label>
        <input type="file" name="image-input" id="image-input" />
        <input type="hidden" name="image-url" id="image-url" />
        <p id="warning" class="error"></p>
        <input id="add-post-submit" type="submit" value="Add Post">
    </form>
</div>
</div>
<input  type="submit" id="add-form-toggler" value="Add Post" onclick="showDiv()" />