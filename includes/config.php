<?php
    define("SITE_TITLE", "php_blog");
    define("DIVIDER", " | ");
    define("ROOT", $_SERVER['DOCUMENT_ROOT']);

    // Auto include classes
    spl_autoload_register(function ($class_name) {
        include 'classes/' . $class_name . '.class.php';
    });

    // DATABASE SETTINGS
    // Path to .env file
    $env_file = $_SERVER['DOCUMENT_ROOT']. '/.env';

    // If .env file is found, add variables with DotEnv class.
    // If not, system env vars will be used.
    if (file_exists($env_file)) {
        (new DotEnv($env_file))->load();
    }

    // Database config from env vars. Can be changed to other values if needed.
    define("DB_HOST", getenv('DB_HOST'));
    define("DB_USER", getenv('DB_USER'));
    define("DB_PASS", getenv('DB_PASS'));
    define("DB_DATABASE", getenv('DB_DATABASE'));

    // Start session
    session_start();    

    // Uncomment for error reporting
    //error_reporting(-1);
    //ini_set("display_errors", 1);
?>