<?php

// ---------------------------------------------------------------------------------------------
// USER HANDLE FUNCTIONS -----------------------------------------------------------------------

function register_user()
{
    $user = new User();
    if (!empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['confirm'])) {
        if ($_POST['password'] == $_POST['confirm']) {
            if ($user->add_user($_POST['username'], $_POST['fullname'], $_POST['password'])) {
                return "User Added! You can now login.";
            } else {
                return "User could not be added.";
            }
        } else {
            return  'Passwords don\'t match.';
        }
    } elseif (empty($_POST['username'])) {
        return 'Username field was empty.';
    } elseif (empty($_POST['password'])) {
        return 'Password field was empty.';
    }
}

function login()
{
    $user = new User();
    if (!empty($_POST['username']) && !empty($_POST['password'])) {

        $user->set_username($_POST['username']);
        $user->set_password($_POST['password']);

        if ($user->verify_password()) {
            $id = $user->get_user_id($user->get_username())['id'];
            $user->set_id($id);
            $_SESSION['username'] = $user->get_username();
            $_SESSION['user_id'] = $user->get_id();
            echo $user->get_id();

            // Route to user page
            header('Location: index.php?page=user&user_id=' . strval($user->get_id()) . '');
            exit();
        } else return 'Wrong user or password.';
    } elseif (empty($_POST['username'])) {
        return 'Username field was empty.';
    } elseif (empty($_POST['password'])) {

        return 'Password field was empty.';
    }
}

// ---------------------------------------------------------------------------------------------
// POSTING FUNCTIONS ---------------------------------------------------------------------------

function add_post()
{
    $post = new Post();

    if (!empty($_POST['title']) && !empty($_POST['body'])) {

        // Uncomment if file upload should be to server.
        // if (!empty($_FILES['image']['name'])) {
        //    $img_url = handle_image_upload($_FILES['image']);

        // Set img url if it's posted.
        if (!empty($_POST['image-url'])) {
            $img_url = $_POST['image-url'];
        } else {
            $img_url = "";
        }

        if ($post->add_post($_POST['title'], $_POST['body'], $img_url, $_SESSION['user_id'])) {
            return "Post Added.";
        } else {
            return "Post could not be added.";
        }

    } elseif (empty($_POST['title'])) {
        return "Title field was empty";
    } elseif (empty($_POST['body'])) {
        return "Post can not be added without content.";
    }
}

// Image uploading
// To use if files can be uploaded to server. 
function handle_image_upload($file)
{
    $file_name = $file['name'];
    $explode = explode('.', $file_name);
    $file_ext = strtolower(end($explode));
    $file_tmp = $file['tmp_name'];
    $allowed_types = array("jpeg", "jpg", "png");
    $file_path = "uploads/" . $file_name;

    if (in_array($file_ext, $allowed_types) === false) {
        return "";
    }

    move_uploaded_file($file_tmp, $file_path);
    return $file_path;
}



function delete_post($id)
{
    $post = new Post($id);
    $comments = new Comment();
    if ($post->get_username() == $_SESSION['username']) {

        // If post has comments they have to be deleted first
        $comments->delete_comments($id);
        if ($post->delete_post($id)) {

            // Uncomment if file uploads are handled on server.
            // if ($post->get_img()) {
            //     delete_image($post->get_img());
            // }

            header('Location: index.php?page=user&user_id=' . strval($_SESSION['user_id']) . '');
            exit();
            //return "";
        } else {
            return "Post could not be deleted.";
        }
    }
}

// Delete image on server.
function delete_image($img)
{
    unlink($img);
}

// Adds comment to post
function add_comment($body)
{
    $comment = new Comment();
    if ($comment->create_comment($body, $_SESSION['user_id'], $_GET['id'])) {
        return "";
    } else {
        return "Comment could not be added.";
    }
}



// ---------------------------------------------------------------------------------------------
// RENDERING FUNCTIONS

function render_userlist()
{
    $user = new User();
    $users = $user->get_users();

    // Quit function if array is empty.
    if (empty($users)) {
        echo "<p>No users yet!</p>";
        return "";
    }

    foreach ($users as $user) {
        echo "<a class='user-link' href='index.php?page=user&user_id=" . $user['id'] . "'>" . $user['username'] . "</a><br>";
    }
}


function render_posts($id = null)
{
    $post = new Post();

    // Get users posts if a user id is called.
    if (!$id) {
        $posts = $post->get_all_posts();
    } else {
        $posts = $post->get_posts($id);
    }
    // Quit function if array is empty.
    if (empty($posts)) {
        echo "<p>No blogposts yet!</p>";
        return "";
    }

    // Show only 5 items if page is index.
    if (isset($_GET['page'])) {
        if ($_GET['page'] == 'index') {
            $length = 5;
        } else {
            $length = count($posts);
        }
    } else {$length = count($posts);}

    // Loop through posts array and display list.
    for ($i = 0; $i < $length; $i++) {
        if (array_key_exists($i, $posts)) {
            $title = $posts[$i]['title'];
            $id = $posts[$i]['id'];
            $date = $posts[$i]['created_at'];
            $user_id = $posts[$i]['user_id'];
            $username = $posts[$i]['username'];
            $fullname = $posts[$i]['fullname'];
            $img = $posts[$i]['img_path'];
            $body_short = substr($posts[$i]['body'], 0, 500) . '...';

            echo
            "<section>
                <h2>" . $title . "</h2>
                <p><em>" . $date . "</em> | Author: " . $fullname . "  (<a href='index.php?page=user&user_id=" . $user_id . "'>" . $username . "</a>)</p>";
            if ($img) {
                echo  "<img class='post-img' src='" . $img . "' alt='Small size image of blog post.'>";
            }
            echo "<p class='no-margin-bottom'>" . $body_short . "</p>
            <a href='index.php?page=blogpost&id=" . $id . "'>Read More</a>";

            // Render edit and delete option if user is logged in
            if (isset($_SESSION['user_id'])) {
                if ($user_id == $_SESSION['user_id']) {
                    echo
                    " | <a href='index.php?page=edit&id=" . $id . "'>Edit</a> | 
                        <a href='index.php?page=user&user_id=" . $user_id . "&deleteid=" . $id . "'>Delete</a>";
                }
            }

            echo "</section>";
        }
    }
}

// Show comments of post with id
function render_comments($id)
{
    $comment = new Comment();
    $comments = $comment->get_post_comments($id);
    foreach ($comments as $comment) {
        echo
        "<p><a class='user-link' href='index.php?page=user&user_id=" . $comment['user_id'] . "'>"
            . $comment['username'] .
            "</a> - " . $comment['body'] . "</p>";
    }
}
