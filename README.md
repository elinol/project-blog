# PHP Project Blog
A simple webapp for a school project.

## Database connection
Connection variables for local environment can be edited in .env (or directly in config.php).

## File Uploads
Please change URL in image upload function in script.js if you're about to use this repo.